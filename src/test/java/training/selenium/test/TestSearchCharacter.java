package training.selenium.test;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import training.selenium.pages.SearchCharacterPage;

public class TestSearchCharacter extends TestInitializer {

    private static SearchCharacterPage searchCharacterPage;

    @BeforeAll
    public static void setupTestClass() {
        searchCharacterPage = new SearchCharacterPage(driver);
    }

    @Test
    public void shouldFindGivenCharacter() {
        // given
        String name = "Galvatronik";

        // when
        searchCharacterPage
                .goTo()
                .typeInCharacterName(name)
                .clickOnSubmitButton();

        // then
        softAssertions.assertThat(searchCharacterPage.getCharacterName())
                .isEqualTo(name);
        softAssertions.assertThat(searchCharacterPage.isCharacterInformationLabelVisible())
                .isTrue();
    }

    @Test
    public void shouldFindCharacterWithNameContainingSpaceBarCharacter() {
        // given
        String name = "Barabas Dantioch";

        // when
        searchCharacterPage
                .goTo()
                .typeInCharacterName(name)
                .clickOnSubmitButton();

        // then
        softAssertions.assertThat(searchCharacterPage.getCharacterName())
                .isEqualTo(name);
        softAssertions.assertThat(searchCharacterPage.isCharacterInformationLabelVisible())
                .isTrue();
    }

    @Test
    public void shouldReturnCharacterDoesNotExistInformation() {
        //given
        String invalidName = RandomStringUtils.randomAlphabetic(10);

        //when
        searchCharacterPage
                .goTo()
                .typeInCharacterName(invalidName)
                .clickOnSubmitButton();

        //then
        softAssertions.assertThat(searchCharacterPage.isCharacterDoesNotExistInfoVisible())
                .isTrue();
        softAssertions.assertThat(searchCharacterPage.isCouldNotFindCharacterTableVisible())
                .isTrue();
    }
}
