package training.selenium.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import training.selenium.pages.library.CreaturesPage;
import training.selenium.pages.news.LatestNewsPage;

public class TestCreaturePage extends TestInitializer {

    private static CreaturesPage creaturesPage;
    private static LatestNewsPage latestNewsPage;

    @BeforeAll
    public static void setupTestClass() {
        creaturesPage = new CreaturesPage(driver);
        latestNewsPage = new LatestNewsPage(driver);
    }


    @Test
    public void loadChosenCreature() {
        // given
        String creatureName = "Crazed Winter Vanguards";

        // when
        latestNewsPage
                .goTo()
                .getNavigationMenu()
                .extendSection("Library")
                .navigateToPage("Creatures");

        creaturesPage
                .clickOnCreatureIcon(creatureName);

        //then
        softAssertions.assertThat(creaturesPage.isCreatureInformationLabelVisible(creatureName))
                .isTrue();

    }
}
