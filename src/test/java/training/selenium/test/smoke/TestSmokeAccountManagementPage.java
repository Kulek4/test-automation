package training.selenium.test.smoke;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import training.selenium.pages.account.AccountManagementPage;
import training.selenium.test.TestInitializer;

public class TestSmokeAccountManagementPage extends TestInitializer {

    private static AccountManagementPage accountManagementPage;

    @BeforeAll
    public static void setupTestClass() {
        accountManagementPage = new AccountManagementPage(driver);
    }

    @Test
    public void shouldVerifyThatBasicElementsAreProperlyDisplayed() {
        //given
        String loginButton = "Login";
        String createAccountButton = "Create Account";
        String facebookSection = "Do you have a Facebook profile?";
        String newToTibia = "New to Tibia?";

        //when
        accountManagementPage.goTo();

        //then
        softAssertions.assertThat(accountManagementPage.getAccountSectionBox().isVisible())
                .isTrue();
        softAssertions.assertThat(accountManagementPage.getCreatureArtwork().isVisible())
                .isTrue();
        softAssertions.assertThat(accountManagementPage.getDownloadBox().isVisible())
                .isTrue();
        softAssertions.assertThat(accountManagementPage.getInfoBar().isVisible())
                .isTrue();
        softAssertions.assertThat(accountManagementPage.getNavigationMenu().isVisible())
                .isTrue();
        softAssertions.assertThat(accountManagementPage.isAccountManagementSectionVisible())
                .isTrue();
        softAssertions.assertThat(accountManagementPage.isButtonOnAccountManagementVisible(loginButton))
                .isTrue();
        softAssertions.assertThat(accountManagementPage.isButtonOnAccountManagementVisible(createAccountButton))
                .isTrue();
        softAssertions.assertThat(accountManagementPage.isSmallerSectionVisible(facebookSection))
                .isTrue();
        softAssertions.assertThat(accountManagementPage.isSmallerSectionVisible(newToTibia))
                .isTrue();
    }

}
