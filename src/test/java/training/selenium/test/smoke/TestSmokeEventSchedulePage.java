package training.selenium.test.smoke;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import training.selenium.pages.news.EventSchedulePage;
import training.selenium.test.TestInitializer;

public class TestSmokeEventSchedulePage extends TestInitializer {

    private static EventSchedulePage eventSchedulePage;

    @BeforeAll
    public static void setupTestClass() {
        eventSchedulePage = new EventSchedulePage(driver);
    }

    @Test
    public void shouldVerifyThatBasicElementsAreProperlyDisplayed() {
        // when
        eventSchedulePage
                .goTo();

        // then
        softAssertions.assertThat(eventSchedulePage.getAccountSectionBox().isVisible())
                .isTrue();
        softAssertions.assertThat(eventSchedulePage.getDownloadBox().isVisible())
                .isTrue();
        softAssertions.assertThat(eventSchedulePage.getNavigationMenu().isVisible())
                .isTrue();
        softAssertions.assertThat(eventSchedulePage.getInfoBar().isVisible())
                .isTrue();
        softAssertions.assertThat(eventSchedulePage.getCreatureArtwork().isVisible())
                .isTrue();
        softAssertions.assertThat(eventSchedulePage.isEventScheduleSectionVisible())
                .isFalse();
    }
}
