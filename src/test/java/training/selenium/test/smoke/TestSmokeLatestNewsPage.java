package training.selenium.test.smoke;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import training.selenium.pages.news.LatestNewsPage;
import training.selenium.test.TestInitializer;

public class TestSmokeLatestNewsPage extends TestInitializer {

    private static LatestNewsPage latestNewsPage;

    @BeforeAll
    public static void setupTestClass() {
        latestNewsPage = new LatestNewsPage(driver);
    }

    @Test
    public void shouldVerifyThatBasicElementsAreProperlyDisplayed() {
        // when
        latestNewsPage
                .goTo();

        // then
        softAssertions.assertThat(latestNewsPage.getAccountSectionBox().isVisible())
                .isTrue();
        softAssertions.assertThat(latestNewsPage.getDownloadBox().isVisible())
                .isTrue();
        softAssertions.assertThat(latestNewsPage.getNavigationMenu().isVisible())
                .isTrue();
        softAssertions.assertThat(latestNewsPage.getInfoBar().isVisible())
                .isTrue();
        softAssertions.assertThat(latestNewsPage.getCreatureArtwork().isVisible())
                .isTrue();
        softAssertions.assertThat(latestNewsPage.isNewsSectionVisible())
                .isTrue();
    }
}
