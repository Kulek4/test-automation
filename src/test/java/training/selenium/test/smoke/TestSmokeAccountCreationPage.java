package training.selenium.test.smoke;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import training.selenium.pages.account.AccountCreationPage;
import training.selenium.test.TestInitializer;

public class TestSmokeAccountCreationPage extends TestInitializer {

    private static AccountCreationPage accountCreationPage;

    @BeforeAll
    public static void setupTestClass() {
        accountCreationPage = new AccountCreationPage(driver);
    }

    @Test
    public void shouldVerifyThatBasicElementsAreProperlyDisplayed() {
        // when
        accountCreationPage
                .goTo();

        // then
        softAssertions.assertThat(accountCreationPage.getAccountSectionBox().isVisible())
                .isTrue();
        softAssertions.assertThat(accountCreationPage.getDownloadBox().isVisible())
                .isTrue();
        softAssertions.assertThat(accountCreationPage.getNavigationMenu().isVisible())
                .isTrue();
        softAssertions.assertThat(accountCreationPage.getInfoBar().isVisible())
                .isTrue();
        softAssertions.assertThat(accountCreationPage.getCreatureArtwork().isVisible())
                .isTrue();
        softAssertions.assertThat(accountCreationPage.isCreateAccountBoxVisible())
                .isTrue();
    }
}
