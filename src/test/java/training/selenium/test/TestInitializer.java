package training.selenium.test;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.openqa.selenium.WebDriver;
import training.selenium.core.driver.DriverManager;
import training.selenium.reporting.AttachmentsExtension;

@ExtendWith({AttachmentsExtension.class})
@Execution(ExecutionMode.CONCURRENT)
public abstract class TestInitializer {

    protected static WebDriver driver;
    protected SoftAssertions softAssertions;

    @BeforeAll
    public static void setupBeforeAll() {
        driver = DriverManager.getDriver();
    }

    @BeforeEach
    public void initAssertions() {
        softAssertions = new SoftAssertions();
    }

    @AfterEach
    public void flushAssertions() {
        softAssertions.assertAll();
    }

    @AfterAll
    public static void tearDownAfterAll() {
        DriverManager.removeWebDriver();
    }
}