package training.selenium.test.e2e;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import training.selenium.pages.community.HighscoreRow;
import training.selenium.pages.community.HighscoresPage;
import training.selenium.pages.news.LatestNewsPage;
import training.selenium.test.TestInitializer;

public class TestHighscoresPage extends TestInitializer {

    private HighscoresPage highscoresPage;
    private LatestNewsPage latestNewsPage;

    @BeforeEach
    public void setup() {
        latestNewsPage = new LatestNewsPage(driver);
        highscoresPage = new HighscoresPage(driver);
    }

    @Test
    public void shouldRetrieveCorrectPlayerData() {
        // when
        latestNewsPage
                .goTo()
                .getNavigationMenu()
                .extendSection("Community")
                .navigateToPage("Highscores");

        highscoresPage
                .selectWorld("Faluna")
                .selectVocation("Druids")
                .selectCategory("Experience Points")
                .clickOnSubmitButton();

        // then
        HighscoreRow firstRow = highscoresPage.getHighscoreRowByNumber(1);

        softAssertions.assertThat(highscoresPage.isHighscoresTableVisible())
                .isTrue();
        softAssertions.assertThat(firstRow.getRank())
                .isEqualTo("1");
        softAssertions.assertThat(firstRow.getName())
                .isEqualTo("Svep en Celsius");
        softAssertions.assertThat(firstRow.getVocation())
                .isEqualTo("Elder Druid");
        softAssertions.assertThat(firstRow.getLevel())
                .isGreaterThanOrEqualTo(1000);
        softAssertions.assertThat(firstRow.getPoints().length())
                .isGreaterThanOrEqualTo(14);

    }

}
