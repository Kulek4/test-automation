package training.selenium.test.e2e;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import training.selenium.components.NavigationMenu;
import training.selenium.pages.community.KillStatisticsPage;
import training.selenium.pages.community.RaceRow;
import training.selenium.pages.news.LatestNewsPage;
import training.selenium.test.TestInitializer;

@Log4j2
public class TestKillStatisticsPage extends TestInitializer {


    private KillStatisticsPage killStatisticsPage;
    private NavigationMenu navigationMenu;
    private LatestNewsPage latestNewsPage;

    @BeforeEach
    public void setup(){
        killStatisticsPage = new KillStatisticsPage(driver);
        navigationMenu = new NavigationMenu(driver);
        latestNewsPage = new LatestNewsPage(driver);
    }

    @Test
    public void verifyKillStatics(){
        //when
        latestNewsPage
                .goTo()
                .getNavigationMenu();

        navigationMenu
                .extendSection("Community")
                .navigateToPage("Kill Statistics");

        killStatisticsPage
                .selectWorld("Furia")
                .clickSubmitButton();
        //then
        RaceRow chosenRow = killStatisticsPage.getRowByRace("Grand Master Oberon");
        log.info("[UG]chosenRow.getKilledStatistic()" + chosenRow.getWeeklyKilledStatistic());
        softAssertions.assertThat(chosenRow.getWeeklyKilledStatistic()).isGreaterThanOrEqualTo(0);
        softAssertions.assertThat(chosenRow.getLastDayKilledByPlayerStatics()).isGreaterThanOrEqualTo(0);
        softAssertions.assertThat(chosenRow.getLastDayKilledStatistics()).isGreaterThanOrEqualTo(0);
        softAssertions.assertThat(chosenRow.getWeeklyKilledByPlayerStatistics()).isGreaterThanOrEqualTo(0);

    }
}
