package training.selenium.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import training.selenium.pages.account.AccountCreationPage;
import training.selenium.pages.account.AccountManagementPage;
import training.selenium.pages.news.LatestNewsPage;

public class TestNavigationToAccountPages extends TestInitializer {

    private static LatestNewsPage latestNewsPage;


    @BeforeAll
    public static void setupTestClass() {
        latestNewsPage = new LatestNewsPage(driver);
    }

    @Test
    public void shouldNavigateToLoginPage() {
        AccountManagementPage accountManagementPage = latestNewsPage
                .goTo()
                .getAccountSectionBox()
                .clickOnLoginButton();

        softAssertions.assertThat(accountManagementPage.isAccountManagementSectionVisible())
                .isTrue();
    }

    @Test
    public void shouldNavigateToAccountCreationPage() {
        AccountCreationPage accountCreationPage = latestNewsPage
                .goTo()
                .getAccountSectionBox()
                .clickOnCreateAccount();

        softAssertions.assertThat(accountCreationPage.isCreateAccountBoxVisible())
                .isTrue();
    }
}
