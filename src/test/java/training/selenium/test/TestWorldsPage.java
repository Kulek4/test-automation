package training.selenium.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import training.selenium.pages.WorldsPage;

public class TestWorldsPage extends TestInitializer {

    private static WorldsPage worldsPage;

    @BeforeAll
    public static void setupTestClass() {
        worldsPage = new WorldsPage(driver);
    }

    @Test
    public void shouldLoadBasicPageElements() {
        // when
        worldsPage
                .goTo();

        // then
        softAssertions.assertThat(worldsPage.isGameWorldOverviewLabelVisible())
                .isTrue();
        softAssertions.assertThat(worldsPage.isRegularWorldsLabelVisible())
                .isTrue();
        softAssertions.assertThat(worldsPage.isTournamentWorldsLabelVisible())
                .isTrue();
        softAssertions.assertThat(worldsPage.isWorldsTableVisible())
                .isTrue();
        softAssertions.assertThat(worldsPage.isOverallMaximumLabelVisible())
                .isTrue();
    }

}
