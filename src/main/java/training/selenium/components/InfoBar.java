package training.selenium.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InfoBar extends AbstractPageObject {

    private static final String INFO_BAR_ROOT_XPATH = "//div[@class='InfoBar']";

    public InfoBar(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return actions.isVisible(By.xpath(INFO_BAR_ROOT_XPATH));
    }
}
