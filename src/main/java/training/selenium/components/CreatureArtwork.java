package training.selenium.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreatureArtwork extends AbstractPageObject {

    private static final String ARTWORK_ROOT_XPATH = "//div[@id='RightArtwork']";

    public CreatureArtwork(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return actions.isVisible(By.xpath(ARTWORK_ROOT_XPATH));
    }
}
