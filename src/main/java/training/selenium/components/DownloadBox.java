package training.selenium.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DownloadBox extends AbstractPageObject {

    private static final String DOWNLOAD_BOX_ROOT_XPATH = "//div[@id='PlayNowContainer']";
    private static final String DOWNLOAD_BUTTON_XPATH = DOWNLOAD_BOX_ROOT_XPATH + "//input[@name='Download']";

    public DownloadBox(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return actions.isVisible(By.xpath(DOWNLOAD_BOX_ROOT_XPATH));
    }

    public void clickOnDownloadButton() {
        actions.click(By.xpath(DOWNLOAD_BUTTON_XPATH));
    }

}
