package training.selenium.components;

import org.openqa.selenium.WebDriver;
import training.selenium.util.ActionsWrapper;

public abstract class AbstractPageObject {

    protected final WebDriver driver;
    protected final ActionsWrapper actions;

    public AbstractPageObject(WebDriver driver) {
        this.driver = driver;
        this.actions = new ActionsWrapper(driver);
    }

}
