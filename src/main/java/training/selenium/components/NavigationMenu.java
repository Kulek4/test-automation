package training.selenium.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NavigationMenu extends AbstractPageObject {

    private static final String NAVIGATION_MENU_ROOT_XPATH = "//div[@id='Menu']";
    private static final String SECTION_XPATH = NAVIGATION_MENU_ROOT_XPATH + "//div[@id='%s']";
    private static final String SECTION_EXPANDER_XPATH = SECTION_XPATH + "//div[@class='Extend']";
    private static final String PAGE_XPATH = NAVIGATION_MENU_ROOT_XPATH + "//div[text()='%s']";

    public NavigationMenu(WebDriver driver) {
        super(driver);
    }

    public NavigationMenu extendSection(String sectionName) {
        By sectionExpanderLocator = By.xpath(String.format(SECTION_EXPANDER_XPATH, sectionName.toLowerCase()));
        String expanderStatus = actions.getAttributeValue(sectionExpanderLocator, "style");
        if (expanderStatus.contains("plus")) {
            actions.click(sectionExpanderLocator);
        }
        return this;
    }

    public void navigateToPage(String pageName) {
        actions.click(By.xpath(String.format(PAGE_XPATH, pageName)));
    }

    public boolean isVisible() {
        return actions.isVisible(By.xpath(NAVIGATION_MENU_ROOT_XPATH));
    }

}
