package training.selenium.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.pages.account.AccountCreationPage;
import training.selenium.pages.account.AccountManagementPage;

public class AccountSectionBox extends AbstractPageObject {

    private static final String MENU_ROOT_XPATH = "//div[@class='SmallMenuBox' and ./div[@id='LoginTop']][1]";
    private static final String LOGIN_BUTTON_XPATH = MENU_ROOT_XPATH + "//input[@name='Login']";
    private static final String CREATE_ACCOUNT_XPATH = MENU_ROOT_XPATH + "//div[@id='LoginstatusText_1']";

    public AccountSectionBox(WebDriver driver) {
        super(driver);
    }

    public AccountManagementPage clickOnLoginButton() {
        actions.click(By.xpath(LOGIN_BUTTON_XPATH));
        return new AccountManagementPage(driver);
    }

    public AccountCreationPage clickOnCreateAccount() {
        actions.click(By.xpath(CREATE_ACCOUNT_XPATH));
        return new AccountCreationPage(driver);
    }

    public boolean isVisible() {
        return actions.isVisible(By.xpath(MENU_ROOT_XPATH));
    }

}
