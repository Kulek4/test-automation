package training.selenium.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitWrapper {

    private static final int DEFAULT_TIMEOUT = 30;
    protected final WebDriver driver;

    WaitWrapper(WebDriver driver) {
        this.driver = driver;
    }

    WebElement untilElementIsVisible(By locator) {
        return new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    WebElement untilElementIsClickable(By locator) {
        return new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.elementToBeClickable(locator));
    }
}
