package training.selenium.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ActionsWrapper {

    protected final WebDriver driver;
    private final WaitWrapper wait;

    public ActionsWrapper(WebDriver driver) {
        this.driver = driver;
        this.wait = new WaitWrapper(driver);
    }

    public void goToUrl(String url) {
        driver.get(url);
    }

    public void typeInValue(By locator, String value) {
        wait.untilElementIsVisible(locator).sendKeys(value);
    }

    public void click(By locator) {
        wait.untilElementIsClickable(locator).click();
    }

    public boolean isVisible(By locator) {
        return wait.untilElementIsVisible(locator).isDisplayed();
    }

    public String getTextValue(By locator) {
        return wait.untilElementIsVisible(locator).getText();
    }

    public String getAttributeValue(By locator, String attribute) {
        return wait.untilElementIsVisible(locator).getAttribute(attribute);
    }

    public void selectOptionFromDropdownByVisibleText(By locator, String option) {
        new Select(retrieveElementWhenVisible(locator)).selectByVisibleText(option);
    }

    private WebElement retrieveElementWhenVisible(By locator) {
        return wait.untilElementIsVisible(locator);
    }

}
