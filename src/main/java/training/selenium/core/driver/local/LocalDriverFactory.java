package training.selenium.core.driver.local;

import io.github.bonigarcia.wdm.DriverManagerType;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import training.selenium.core.driver.IDriver;

@Log4j2
public class LocalDriverFactory implements IDriver {

//    @Override
//    public WebDriver createInstance(String browser) {
//        WebDriver driver = null;
//
//        try {
//            DriverManagerType driverManagerType = DriverManagerType.valueOf(browser.toUpperCase());
//            Class<?> driverClass = Class.forName(driverManagerType.browserClass());
//            WebDriverManager.getInstance(driverManagerType).setup();
//            driver = (WebDriver) driverClass.newInstance();
//            log.info("Prepared a proper driver instance! Running " + browser);
//            maximizeWindow(driver);
//            clearCookies(driver);
//        } catch (IllegalAccessException | ClassNotFoundException e) {
//            log.error("The class could not be found.", e);
//        } catch (InstantiationException e) {
//            log.error("There was a problem while instantiating driver.", e);
//        }
//        return driver;
//    }

    @Override
    public WebDriver createInstance(String browser) {
        WebDriver driver;
        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("window-size=1200,1100");

        driver = new ChromeDriver(options);
//        maximizeWindow(driver);
        clearCookies(driver);
        return driver;
    }

    private void maximizeWindow(WebDriver driver) {
        driver.manage().window().maximize();
        log.info("Browser window maximized.");
    }

    private void clearCookies(WebDriver driver) {
        driver.manage().deleteAllCookies();
        log.info("Browser cookies have been cleared.");
    }
}
