package training.selenium.core.driver;

import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;

import static java.lang.Thread.currentThread;
import static training.selenium.core.driver.DriverFactory.createInstance;

@Log4j2
public class DriverManager {

    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    private DriverManager() {}

    public static WebDriver getDriver() {
        if (driver.get() == null) {
            log.info("Creating instance of WebDriver with thread id " + Thread.currentThread().getId());
            setDriver(createInstance());
        }
        return driver.get();
    }

    public static void removeWebDriver() {
        try {
            quitDriver();
        } catch (Exception e) {
            log.info("Something went wrong! Please check Removal of WebDriver!");
            log.info(Arrays.toString(e.getStackTrace()));
        } finally {
            log.info("Removing WebDriver with id " + currentThread().getName());
            driver.remove();
            log.info("Removed WebDriver");
        }
    }

    private static void quitDriver() {
        DriverManager.driver.get().quit();
        log.info("Quited WebDriver instance with id: " + Thread.currentThread().getId());
    }

    private static void setDriver(WebDriver driver) {
        DriverManager.driver.set(driver);
    }

}
