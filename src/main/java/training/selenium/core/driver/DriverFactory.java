package training.selenium.core.driver;

import lombok.extern.log4j.Log4j2;
import org.aeonbits.owner.ConfigCache;
import org.openqa.selenium.WebDriver;
import training.selenium.core.Configuration;
import training.selenium.core.driver.local.LocalDriverFactory;
import training.selenium.core.driver.remote.RemoteDriverFactory;

@Log4j2
public class DriverFactory {

    private static Configuration configuration = ConfigCache.getOrCreate(Configuration.class);

    public static WebDriver createInstance() {
        configuration = ConfigCache.getOrCreate(Configuration.class);
        Target target = Target.valueOf(configuration.target().toUpperCase());
        WebDriver webdriver;

        switch (target) {

            case LOCAL:
                log.info("Setting up a local driver instance.");
                webdriver = new LocalDriverFactory().createInstance(getLocalBrowser());
                break;
            case GRID:
                log.info("Setting up a remote driver instance");
                webdriver = new RemoteDriverFactory().createInstance(getRemoteBrowser());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + target);
        }

        return webdriver;
    }

    enum Target {
        LOCAL, GRID
    }

    private static String getLocalBrowser() {
        return configuration.getLocalBrowser();
    }

    private static String getRemoteBrowser() {
        return configuration.getRemoteBrowser();
    }
}
