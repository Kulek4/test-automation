package training.selenium.core;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:test.properties"})
public interface Configuration extends Config {

    String target();

    @Key("grid.url")
    String gridUrl();

    @Key("grid.port")
    String gridPort();

    @Key("local.browser")
    String getLocalBrowser();

    @Key("remote.browser")
    String getRemoteBrowser();

}
