package training.selenium.reporting;

import io.qameta.allure.Attachment;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;

import static java.util.stream.Collectors.joining;
import static training.selenium.core.driver.DriverManager.getDriver;

@Log4j2
public class AttachmentsExtension implements TestWatcher {

    @Override
    public void testSuccessful(ExtensionContext context) {
        log.info("Test " + context.getDisplayName() + " has been successful.");
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        log.info("Test " + context.getDisplayName() + "has failed!");
        makeScreenshotOnFailure();
        extractBrowserLogs();
        extractDriverLogs();
    }

    @Attachment(value = "browser.log", type = "text/plain")
    String extractBrowserLogs() {
        return extractLogs(LogType.BROWSER);
    }

    @Attachment(value = "driver.log", type = "text/plain")
    String extractDriverLogs() {
        return extractLogs(LogType.DRIVER);
    }

    @Attachment(value = "Error screenshot", type = "image/png")
    private byte[] makeScreenshotOnFailure() {
        return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
    }

    private String extractLogs(String type){
        LogEntries logEntries = getDriver().manage().logs().get(type);
        return logEntries.getAll().stream().map(LogEntry::toString).collect(joining("\n"));
    }
}
