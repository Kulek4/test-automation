package training.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchCharacterPage extends BasePage<SearchCharacterPage> {

    private static final String NAME_INPUT_XPATH = "//input[@name='name']";
    private static final String SUBMIT_BUTTON_XPATH = "//input[@name='Submit']";
    private static final String CHAR_INFO_TABLE_ROOT_XPATH = "//table[contains(.,'Character Information')]";
    private static final String CHARACTER_NAME_CELL_XPATH = CHAR_INFO_TABLE_ROOT_XPATH +  "//td[contains(.,'Name:')]/following-sibling::td";
    private static final String CHARACTER_INFORMATION_LABEL_XPATH = CHAR_INFO_TABLE_ROOT_XPATH + "//b";

    private static final String COULD_NOT_FIND_CHARACTER_TABLE_XPATH = "//table[contains(.,'Could not find character')]";
    private static final String CHARACTER_DOES_NOT_EXIST_INFO_TEXT_XPATH = "(" + COULD_NOT_FIND_CHARACTER_TABLE_XPATH + "//td[./b[text()]])[2]";

    public SearchCharacterPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String configURL() {
        return "community/?subtopic=characters";
    }

    public SearchCharacterPage typeInCharacterName(String name) {
        actions.typeInValue(By.xpath(NAME_INPUT_XPATH), name);
        return this;
    }

    public SearchCharacterPage clickOnSubmitButton() {
        actions.click(By.xpath(SUBMIT_BUTTON_XPATH));
        return this;
    }

    public String getCharacterName() {
        return actions.getTextValue(By.xpath(CHARACTER_NAME_CELL_XPATH));
    }

    public String getCharacterDoesNotExistText() {
        return actions.getTextValue(By.xpath(CHARACTER_DOES_NOT_EXIST_INFO_TEXT_XPATH));
    }

    public boolean isCharacterInformationLabelVisible() {
        return actions.isVisible(By.xpath(CHARACTER_INFORMATION_LABEL_XPATH));
    }

    public boolean isCouldNotFindCharacterTableVisible() {
        return actions.isVisible(By.xpath(COULD_NOT_FIND_CHARACTER_TABLE_XPATH));
    }

    public boolean isCharacterDoesNotExistInfoVisible() {
        return actions.isVisible(By.xpath(CHARACTER_DOES_NOT_EXIST_INFO_TEXT_XPATH));
    }
}
