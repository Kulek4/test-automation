package training.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.components.NavigationMenu;

public class WorldsPage extends BasePage<WorldsPage> {

    private static final String WORLDS_TABLE_ROOT_XPATH = "//table[@class='Table3']";
    private static final String GAME_WORLD_OVERVIEW_LABEL_XPATH ="//div[text()='Game World Overview']";
    private static final String TOURNAMENT_WORLDS_LABEL_XPATH = "//td[text()='Tournament Worlds']";
    private static final String REGULAR_WORLDS_LABEL_XPATH = "//td[text()='Regular Worlds']";
    private static final String OVERALL_MAXIMUM_LABEL_PATH = WORLDS_TABLE_ROOT_XPATH + "//table[@class='TableContent']//td";

    private static final String WORLD_ROW_XPATH = "//tr[(@class) and .//a[contains(text(),'%s')]]";
    private static final String WORLD_NAME_LINK_XPATH = WORLD_ROW_XPATH + "/td[1]/a";
    private static final String WORLD_ONLINE_INFO_XPATH = WORLD_ROW_XPATH + "/td[2]";
    private static final String WORLD_LOCATION_XPATH = WORLD_ROW_XPATH + "/td[3]";
    private static final String WORLD_PVP_TYPE_XPATH = WORLD_ROW_XPATH + "/td[4]";
    private static final String WORLD_BATTLE_EYE_XPATH = WORLD_ROW_XPATH + "/td[5]//img";
    private static final String WORLD_ADDITIONAL_INFO_XPATH = WORLD_ROW_XPATH + "/td[6]";

    private NavigationMenu navigationMenu;

    public WorldsPage(WebDriver driver) {
        super(driver);
        this.navigationMenu = new NavigationMenu(driver);
    }

    @Override
    protected String configURL() {
        return "community/?subtopic=worlds";
    }

    public boolean isWorldsTableVisible() {
        return actions.isVisible(By.xpath(WORLDS_TABLE_ROOT_XPATH));
    }

    public boolean isGameWorldOverviewLabelVisible() {
        return actions.isVisible(By.xpath(GAME_WORLD_OVERVIEW_LABEL_XPATH));
    }

    public boolean isTournamentWorldsLabelVisible() {
        return actions.isVisible(By.xpath(TOURNAMENT_WORLDS_LABEL_XPATH));
    }

    public boolean isRegularWorldsLabelVisible() {
        return actions.isVisible(By.xpath(REGULAR_WORLDS_LABEL_XPATH));
    }

    public boolean isOverallMaximumLabelVisible() {
        return actions.isVisible(By.xpath(OVERALL_MAXIMUM_LABEL_PATH));
    }

    public NavigationMenu getNavigationMenu() {
        return navigationMenu;
    }
}
