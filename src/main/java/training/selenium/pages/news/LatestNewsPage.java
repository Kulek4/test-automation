package training.selenium.pages.news;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.pages.BasePage;

public class LatestNewsPage extends BasePage<LatestNewsPage> {

    private static final String NEWS_SECTION_XPATH = "//div[@id='News']";

    public LatestNewsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String configURL() {
        return "news/?subtopic=latestnews";
    }

    public boolean isNewsSectionVisible() {
        return actions.isVisible(By.xpath(NEWS_SECTION_XPATH));
    }

}
