package training.selenium.pages.news;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.pages.BasePage;

public class EventSchedulePage extends BasePage<EventSchedulePage> {

    private static final String EVENT_SCHEDULE_SECTION_XPATH = "//div[@id='EventSchedule']";

    public EventSchedulePage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String configURL() {
        return "news/?subtopic=eventcalendar";
    }

    public boolean isEventScheduleSectionVisible() {
        return actions.isVisible(By.xpath(EVENT_SCHEDULE_SECTION_XPATH));
    }
}
