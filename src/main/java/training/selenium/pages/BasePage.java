package training.selenium.pages;

import org.openqa.selenium.WebDriver;
import training.selenium.components.*;

public abstract class BasePage<T extends BasePage> extends AbstractPageObject {

    private static final String BASE_URL = "https://www.tibia.com/";

    private final AccountSectionBox accountSectionBox;
    private DownloadBox downloadBox;
    private NavigationMenu navigationMenu;
    private InfoBar infoBar;
    private CreatureArtwork creatureArtwork;

    public BasePage(WebDriver driver) {
        super(driver);
        this.accountSectionBox = new AccountSectionBox(driver);
        this.downloadBox = new DownloadBox(driver);
        this.navigationMenu = new NavigationMenu(driver);
        this.infoBar = new InfoBar(driver);
        this.creatureArtwork = new CreatureArtwork(driver);
    }

    protected abstract String configURL();

    public T goTo() {
        actions.goToUrl(BASE_URL + configURL());
        return (T) this;
    }

    public AccountSectionBox getAccountSectionBox() {
        return accountSectionBox;
    }

    public DownloadBox getDownloadBox() {
        return downloadBox;
    }

    public NavigationMenu getNavigationMenu() {
        return navigationMenu;
    }

    public InfoBar getInfoBar() {
        return infoBar;
    }

    public CreatureArtwork getCreatureArtwork() {
        return creatureArtwork;
    }
}
