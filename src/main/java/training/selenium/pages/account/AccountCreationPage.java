package training.selenium.pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.pages.BasePage;

public class AccountCreationPage extends BasePage<AccountCreationPage> {

    private static final String CREATE_ACCOUNT_BOX_XPATH = "//div[@id='createaccount']";

    public AccountCreationPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String configURL() {
        return "account/?subtopic=createaccount";
    }

    public boolean isCreateAccountBoxVisible() {
        return actions.isVisible(By.xpath(CREATE_ACCOUNT_BOX_XPATH));
    }

}
