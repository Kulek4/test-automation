package training.selenium.pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.pages.BasePage;

public class AccountManagementPage extends BasePage<AccountManagementPage> {

    private static final String ACCOUNT_MANAGEMENT_SECTION_XPATH = "//div[@id='accountmanagement']";
    private static final String ACCOUNT_MANAGEMENT_BUTTONS_XPATH =
            ACCOUNT_MANAGEMENT_SECTION_XPATH + "//input[@name='%s']";
    private static final String ACCOUNT_MANAGEMENT_SMALL_SECTIONS_XPATH =
            ACCOUNT_MANAGEMENT_SECTION_XPATH + "//h1[contains(text(),'%s')]";

    public AccountManagementPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String configURL() {
        return "account/?subtopic=accountmanagement";
    }

    public boolean isAccountManagementSectionVisible() {
        return actions.isVisible(By.xpath(ACCOUNT_MANAGEMENT_SECTION_XPATH));
    }

    public boolean isButtonOnAccountManagementVisible (String buttonName) {
        return actions.isVisible(By.xpath(String.format(ACCOUNT_MANAGEMENT_BUTTONS_XPATH,buttonName)));
    }

    public boolean isSmallerSectionVisible (String sectionName) {
        return actions.isVisible((By.xpath(String.format
                (ACCOUNT_MANAGEMENT_SMALL_SECTIONS_XPATH,sectionName))));
    }

}
