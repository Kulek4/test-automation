package training.selenium.pages.library;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.pages.BasePage;

public class CreaturesPage extends BasePage<CreaturesPage> {

    private static final String CREATURE_ICON_XPATH = "//div[@id='creatures']//div[text()='%s']/../a";
    private static final String CREATURE_INFORMATION_LABEL_XPATH = "//div[@id='creatures']//h2[text()='%s']";

    public CreaturesPage(WebDriver driver) {
        super(driver);
    }

    protected String configURL() {
        return "library/?subtopic=creatures";
    }

    public CreaturesPage clickOnCreatureIcon(String creatureName) {
        actions.click(By.xpath(String.format(CREATURE_ICON_XPATH, creatureName)));
        return this;
    }

    public boolean isCreatureInformationLabelVisible(String creatureName) {
        return actions.isVisible(By.xpath(String.format(CREATURE_INFORMATION_LABEL_XPATH,creatureName)));
    }

}
