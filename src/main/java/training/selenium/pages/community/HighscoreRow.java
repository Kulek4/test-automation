package training.selenium.pages.community;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.components.AbstractPageObject;

public class HighscoreRow extends AbstractPageObject {

    private static String HIGHSCORES_TABLE_XPATH = "//div[@class='TableContainer' and .//text()='Highscores']//table[@class='TableContent']";
    private static String SPECIFIC_ROW_XPATH = HIGHSCORES_TABLE_XPATH + "//tr[.//td[text()='%s']]";

    private static final String RANK_XPATH = "/td[1]";
    private static final String NAME_XPATH = "/td[2]";
    private static final String VOCATION_XPATH = "/td[3]";
    private static final String LEVEL_XPATH = "/td[4]";
    private static final String POINTS_XPATH= "/td[5]";

    private String specificRowXpath;

    public HighscoreRow(WebDriver driver, int rowNumber) {
        super(driver);
        this.specificRowXpath = String.format(SPECIFIC_ROW_XPATH, String.valueOf(rowNumber));
    }

    public String getRank() {
        return actions.getTextValue(By.xpath(specificRowXpath + RANK_XPATH));
    }

    public String getName() {
        return actions.getTextValue(By.xpath(specificRowXpath + NAME_XPATH));
    }

    public String getVocation() {
        return actions.getTextValue(By.xpath(specificRowXpath + VOCATION_XPATH));
    }

    public int getLevel() {
        return Integer.valueOf(actions.getTextValue(By.xpath(specificRowXpath + LEVEL_XPATH)));
    }

    public String getPoints() {
        return actions.getTextValue(By.xpath(specificRowXpath + POINTS_XPATH));
    }

}
