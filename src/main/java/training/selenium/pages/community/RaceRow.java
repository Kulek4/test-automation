package training.selenium.pages.community;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.components.AbstractPageObject;

public class RaceRow extends AbstractPageObject {

    private final String rowXpath;
    private String ROW_XPATH = "//tr[.//td[contains(text(),'%s')]]";

    private static final String NUMBER_OF_KILLED_LAST_DAY_XPATH = "//td[2]";
    private static final String NUMBER_OF_KILLED_BY_PLAYERS_LAST_DAY_XPATH = "//td[3]";
    private static final String NUMBER_OF_KILLED_WEEKLY_XPATH = "//td[4]";
    private static final String NUMBER_OF_KILLED_BY_PLAYERS_WEEKLY_XPATH = "//td[5]";


    public RaceRow(WebDriver driver, String race) {
        super(driver);
        this.rowXpath = String.format(ROW_XPATH, race);
    }

    public int getWeeklyKilledByPlayerStatistics(){
        return Integer.parseInt(actions.getTextValue(By.xpath(rowXpath + NUMBER_OF_KILLED_BY_PLAYERS_WEEKLY_XPATH)).trim());
    }

    public int getWeeklyKilledStatistic(){
        return Integer.parseInt(actions.getTextValue(By.xpath(rowXpath + NUMBER_OF_KILLED_WEEKLY_XPATH)).trim());
    }

    public int getLastDayKilledByPlayerStatics(){
        return Integer.parseInt(actions.getTextValue(By.xpath(rowXpath + NUMBER_OF_KILLED_BY_PLAYERS_LAST_DAY_XPATH)).trim());
    }

    public int getLastDayKilledStatistics(){
        return Integer.parseInt(actions.getTextValue(By.xpath(rowXpath + NUMBER_OF_KILLED_LAST_DAY_XPATH)).trim());
    }
}
