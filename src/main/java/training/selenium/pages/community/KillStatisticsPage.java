package training.selenium.pages.community;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import training.selenium.pages.BasePage;

@Log4j2
public class KillStatisticsPage extends BasePage<KillStatisticsPage> {

    private String WORLD_SELECTION_XPATH = "//table[@class='Table1']//select[@name='world']";
    private String SUBMIT_BUTTON_XPATH = "//div[@class='BigButton']//input[@name='Submit']";

    public KillStatisticsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String configURL() {
        return "community/?subtopic=killstatistics";
    }

    public KillStatisticsPage selectWorld(String worldName){
        actions.isVisible(By.xpath(WORLD_SELECTION_XPATH));
        new Select(driver.findElement(By.xpath(WORLD_SELECTION_XPATH))).selectByValue(worldName);
        return this;
    }

    public KillStatisticsPage clickSubmitButton(){
        actions.click(By.xpath(SUBMIT_BUTTON_XPATH));
        return this;
    }

    public RaceRow getRowByRace(String race) {
        return new RaceRow(driver, race);
    }
}
