package training.selenium.pages.community;

import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import training.selenium.pages.BasePage;

@Log4j2
public class HighscoresPage extends BasePage<HighscoresPage> {

    private static final String FILTER_SECTION_ROOT_XPATH = "//div[@class='CaptionContainer' and .//text()='Highscores Filter']/following-sibling::table";
    private static final String WORLD_DROPDOWN_XPATH = FILTER_SECTION_ROOT_XPATH + "//select[@name='world']";
    private static final String VOCATION_DROPDOWN_XPATH = FILTER_SECTION_ROOT_XPATH + "//select[@name='profession']";
    private static final String CATEGORY_DROPDOWN_XPATH = FILTER_SECTION_ROOT_XPATH + "//select[@name='list']";
    private static final String SUBMIT_BUTTON_XPATH = FILTER_SECTION_ROOT_XPATH + "//input[@name='Submit']";
    private static final String HIGHSCORES_TABLE_XPATH = "//div[@class='TableContainer' and .//text()='Highscores']//table[@class='TableContent']";

    public HighscoresPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String configURL() {
        return "community/?subtopic=highscores";
    }

    public HighscoresPage selectWorld(String world) {
        actions.selectOptionFromDropdownByVisibleText(By.xpath(WORLD_DROPDOWN_XPATH), world);
        return this;
    }

    public HighscoresPage selectVocation(String vocation) {
        actions.selectOptionFromDropdownByVisibleText(By.xpath(VOCATION_DROPDOWN_XPATH), vocation);
        return this;
    }

    public HighscoresPage selectCategory(String category) {
        actions.selectOptionFromDropdownByVisibleText(By.xpath(CATEGORY_DROPDOWN_XPATH), category);
        return this;
    }

    public HighscoresPage clickOnSubmitButton() {
        actions.click(By.xpath(SUBMIT_BUTTON_XPATH));
        return this;
    }

    public HighscoreRow getHighscoreRowByNumber(int rowNumber) {
        return new HighscoreRow(driver, rowNumber);
    }

    public boolean isHighscoresTableVisible() {
        return actions.isVisible(By.xpath(HIGHSCORES_TABLE_XPATH));
    }

}
